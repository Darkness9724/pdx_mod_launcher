OUT := mod_loader
default: clean copy

copy: mod_loader.sh
	chmod +x $^
	cp $^ $(HOME)/.local/bin/$(OUT)
	export PATH=$(PATH):$(HOME)/.local/bin

clean:
	$(RM) $(HOME)/.local/bin/$(OUT)
