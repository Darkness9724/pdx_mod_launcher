#!/bin/sh
#Mod Loader for the new PDX launcher by Darkness9724
#GNU General Public License

display_help() {
    echo "Usage: `basename $0` [.mod file or OPTIONS]" >&2
    echo "-h, --help    show this help and exit"
    echo "-a, --all     load all .mod files"
}

mod_loader() {
    id=$(uuidgen)
    prev=$(cat ./mods_registry.json|sed '$d')
    name=$(grep -oP 'name=\K[^\b]+' $f)
    path=$(grep -v "replace_path" $f | grep -oP '(path|archive)=\K[^\b]+')
    version=$(grep -oP 'supported_version=\K[^\b]+' $f)
    if [ "$version" == "" ]; then
        version="\"2.5.*\"";
    fi
    echo "$prev, \"$id\": {
        \"gameRegistryId\": \"$f\", 
        \"source\": \"local\", 
        \"displayName\": $name, 
        \"dirPath\": $path, 
        \"status\": \"ready_to_play\", 
        \"requiredVersion\": $version,
        \"id\": \"$id\"
        }
    }" > ./mods_registry.json
}

all_mod_loader() {
    echo "{" > ./mods_registry.json
    for f in mod/*.mod; do
        id=$(uuidgen)
        name=$(grep -oP 'name=\K[^\b]+' $f)
        path=$(grep -v "replace_path" $f | grep -oP '(path|archive)=\K[^\b]+')
        version=$(grep -oP 'supported_version=\K[^\b]+' $f)
        version="\"2.5.*\"";
        if [ "$version" == "" ]; then
            version="\"2.5.*\"";
        fi
        echo "\"$id\": {
            \"gameRegistryId\": \"$f\", 
            \"source\": \"local\", 
            \"displayName\": $name, 
            \"dirPath\": $path, 
            \"status\": \"ready_to_play\",
            \"requiredVersion\": $version,
            \"id\": \"$id\"
            }," >> ./mods_registry.json;
    done
    sed -i '$s/,$//' ./mods_registry.json
    echo "}" >> ./mods_registry.json
}

while :
do
    case "$1" in
        -h | --help)
          display_help
          exit 0
          ;;
        -a | --all)
          all_mod_loader
          exit 0
          ;;
        -*)
          echo "Error: Unknown option: $1" >&2
          ## or call function display_help
          exit 1
          ;;
        *)
          f=$1
          mod_loader
	  exit 0
          ;;
    esac
done
